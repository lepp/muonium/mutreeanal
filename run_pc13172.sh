#!/bin/bash

script_path="$(readlink -e "$(dirname "${BASH_SOURCE[0]}")")"
anal_path="${script_path}/build/MuTreeAnal"

. "/home/muX/miniconda3/etc/profile.d/conda.sh"
conda activate py3root

env -u LD_LIBRARY_PATH -u LIBPATH -u DYLD_LIBRARY_PATH -u PYTHONPATH -u SHLIB_PATH "${anal_path}" "${@}"
