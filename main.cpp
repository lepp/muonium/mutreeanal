#include <array>
#include <cerrno>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include <ROOT/RDataFrame.hxx>
#include <ROOT/TThreadedObject.hxx>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TROOT.h>

#include "Cuts.h"
#include "Detector.h"


#define T_POSITRON_COINCIDENCE_MIN -40
#define T_POSITRON_COINCIDENCE_MAX 40
#define T_POSITRON_COINCIDENCE_HISTO_NBINS 22
#define T_POSITRON_COINCIDENCE_HISTO_MIN -44
#define T_POSITRON_COINCIDENCE_HISTO_MAX 44
#define T_NBINS 3000
#define T_MIN -2e3
#define T_MAX 1e4
#define E_NBINS 16384
// Reduce number of bins for 2D histos to avoid memory explosion.
// Furthermore, ROOT still doesn't like very large TObjects.
#define E2D_NBINS 1024
#define E_MIN 0
#define E_MAX 16384


const std::array<std::pair<int, std::string>, 3> cutSequence{{
                                                                     {0, "noEcut"},
                                                                     {E_LOCUT, "lowEcut"},
                                                                     {(E_LOCUT | E_HICUT), "bandEcut"}
                                                             }};


std::pair<std::map<Mu::PositronDetector, double>, std::map<double, Mu::PositronDetector>>
createPos2HistoMap() {
    std::map<Mu::PositronDetector, double> pos2histo;
    std::map<double, Mu::PositronDetector> histo2pos;
    double histoIdx = 0.;
    for (std::size_t dir = Mu::LeftDetector; dir <= Mu::RightDetector; ++dir) {
        for (std::size_t l = 1; l <= N_POSITRON_LAYERS; ++l) {
            for (std::size_t dis = Mu::CloseDetector; dis <= Mu::FarDetector; ++dis) {
                pos2histo[{l, static_cast<Mu::DetectorDirection>(dir),
                           static_cast<Mu::DetectorDistance>(dis)}] = histoIdx;
                ++histoIdx;
            }
        }
    }
    for (const auto &[pos, histo]: pos2histo) {
        histo2pos[histo] = pos;
    }

    return {pos2histo, histo2pos};
}


template<typename TH>
void labelYaxis(TH &histo, std::vector<std::string> &labels) {
    std::size_t bin = 1;
    for (const auto &label: labels) {
        histo->GetYaxis()->SetBinLabel(bin, label.c_str());
        ++bin;
    }
}


void
muTreeAnal(const unsigned numThreads, const std::vector<std::string> &in_filenames, const std::string &out_filename,
           const int cuts) {
    Mu::DetectorMaps detectorMaps;
    detectorMaps.Print();
    Mu::EnergyCuts energyCuts;
    energyCuts.Print();
    const auto &[pos2histo, histo2pos] = createPos2HistoMap();
    const std::size_t nPosDet = pos2histo.size();
    const double posDetBinMin = -.5;
    const double posDetBinMax = static_cast<double>(nPosDet) - .5;
    std::vector<std::string> histoPosDetBinLabels;
    for (const auto&[pos, histo]: pos2histo) {
        histoPosDetBinLabels.emplace_back(Mu::DetectorMaps::GetPosName(pos));
    }
    std::vector<std::string> histoEntranceLabel{"Entrance"};
    std::vector<std::string> histoAtomicLabel{"Atomic"};

    ROOT::EnableThreadSafety();
    ROOT::EnableImplicitMT(numThreads);

    const std::string timeAxis = "t [ns]";
    const std::string energyAxis = "E [ADC units]";

    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiffPPnoVeto("hElectronVsEntranceDiffPPnoVeto", "", T_NBINS, T_MIN,
                                                                T_MAX, nPosDet, posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiffPPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiffPPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CoincidencePPnoVeto(
            "hElectronVsEntranceDiff_CoincidencePPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet, posDetBinMin,
            posDetBinMax);
    hElectronVsEntranceDiff_CoincidencePPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CoincidencePPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic(
            "hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic", "", T_NBINS, T_MIN, T_MAX, nPosDet, posDetBinMin,
            posDetBinMax);
    hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto(
            "hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet,
            posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto(
            "hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet,
            posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto(
            "hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet,
            posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto(
            "hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet,
            posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto(
            "hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet,
            posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto(
            "hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet,
            posDetBinMin, posDetBinMax);
    hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hElectronEnergyPPnoVeto("hElectronEnergyPPnoVeto", "", E_NBINS, E_MIN, E_MAX, nPosDet,
                                                        posDetBinMin, posDetBinMax);
    hElectronEnergyPPnoVeto->GetXaxis()->SetTitle(energyAxis.c_str());
    labelYaxis(hElectronEnergyPPnoVeto, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH3F> hElectronEnergyVsEntranceDiffPPnoVeto("hElectronEnergyVsEntranceDiffPPnoVeto", "",
                                                                      T_NBINS, T_MIN, T_MAX, nPosDet, posDetBinMin,
                                                                      posDetBinMax, E2D_NBINS, E_MIN, E_MAX);
    hElectronEnergyVsEntranceDiffPPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronEnergyVsEntranceDiffPPnoVeto, histoPosDetBinLabels);
    hElectronEnergyVsEntranceDiffPPnoVeto->GetZaxis()->SetTitle(energyAxis.c_str());
    ROOT::TThreadedObject<TH3F> hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto(
            "hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto", "", T_NBINS, T_MIN, T_MAX, nPosDet, posDetBinMin,
            posDetBinMax, E2D_NBINS, E_MIN, E_MAX);
    hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto, histoPosDetBinLabels);
    hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto->GetZaxis()->SetTitle(energyAxis.c_str());
    ROOT::TThreadedObject<TH3F> hElectronVsElectronEnergy_CoincidencePPnoVeto(
            "hElectronVsElectronEnergy_CoincidencePPnoVeto", "", E2D_NBINS, E_MIN, E_MAX, nPosDet, posDetBinMin,
            posDetBinMax, E2D_NBINS, E_MIN, E_MAX);
    hElectronVsElectronEnergy_CoincidencePPnoVeto->GetXaxis()->SetTitle(("F " + energyAxis).c_str());
    labelYaxis(hElectronVsElectronEnergy_CoincidencePPnoVeto, histoPosDetBinLabels);
    hElectronVsElectronEnergy_CoincidencePPnoVeto->GetZaxis()->SetTitle(("C " + energyAxis).c_str());
    ROOT::TThreadedObject<TH2F> hElectronVsElectronTime_CoincidencePPnoVeto(
            "hElectronVsElectronTime_CoincidencePPnoVeto", "", T_POSITRON_COINCIDENCE_HISTO_NBINS,
            T_POSITRON_COINCIDENCE_HISTO_MIN, T_POSITRON_COINCIDENCE_HISTO_MAX, nPosDet, posDetBinMin, posDetBinMax);
    hElectronVsElectronTime_CoincidencePPnoVeto->GetXaxis()->SetTitle(("F-C " + timeAxis).c_str());
    labelYaxis(hElectronVsElectronTime_CoincidencePPnoVeto, histoPosDetBinLabels);
    // The online display expects these histos two be 2D in case of multiple detectors.
    // We have only one, so just hard-code it for the first bin to avoid the need for another detector map.
    ROOT::TThreadedObject<TH2F> hAtomicEnergyPPnoVeto("hAtomicEnergyPPnoVeto", "", E_NBINS, E_MIN, E_MAX, 1, -.5, .5);
    hAtomicEnergyPPnoVeto->GetXaxis()->SetTitle(energyAxis.c_str());
    labelYaxis(hAtomicEnergyPPnoVeto, histoAtomicLabel);
    ROOT::TThreadedObject<TH2F> hAtomicElectronVsEntranceDiffPPnoVeto("hAtomicElectronVsEntranceDiffPPnoVeto", "",
                                                                      T_NBINS, T_MIN, T_MAX, 1, -.5, .5);
    hAtomicElectronVsEntranceDiffPPnoVeto->GetXaxis()->SetTitle(timeAxis.c_str());
    labelYaxis(hAtomicElectronVsEntranceDiffPPnoVeto, histoAtomicLabel);
    ROOT::TThreadedObject<TH2F> hMuonEnergyPPnoVeto("hMuonEnergyPPnoVeto", "", E_NBINS, E_MIN, E_MAX, 1, -.5, .5);
    hMuonEnergyPPnoVeto->GetXaxis()->SetTitle(energyAxis.c_str());
    labelYaxis(hMuonEnergyPPnoVeto, histoEntranceLabel);
    auto fillPPnoVetoHistos = [
            &detectorMaps,
            &energyCuts,
            &cuts,
            &pos2histo,
            &hElectronVsEntranceDiffPPnoVeto,
            &hElectronVsEntranceDiff_CoincidencePPnoVeto,
            &hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic,
            &hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto,
            &hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto,
            &hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto,
            &hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto,
            &hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto,
            &hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto,
            &hElectronEnergyPPnoVeto,
            &hElectronEnergyVsEntranceDiffPPnoVeto,
            &hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto,
            &hElectronVsElectronEnergy_CoincidencePPnoVeto,
            &hElectronVsElectronTime_CoincidencePPnoVeto,
            &hAtomicEnergyPPnoVeto,
            &hAtomicElectronVsEntranceDiffPPnoVeto,
            &hMuonEnergyPPnoVeto
    ](
            const int runNumber,
            const std::vector<unsigned short> &electronModules,
            const std::vector<unsigned short> &electronChannels,
            const std::vector<float> &electronTimes,
            const std::vector<float> &electronEnergies,
            const std::vector<float> &atomicTimes,
            const std::vector<float> &atomicEnergies,
            const std::vector<float> &muonEnergies
    ) {
        const auto &dm = detectorMaps.GetMap(runNumber);
        const auto &ec = energyCuts.GetCut(runNumber);
        // Loop through far detectors and find coincidences with close detectors
        for (std::size_t idx1 = 0; idx1 < electronModules.size(); ++idx1) {
            const Mu::SisDetector sis1{electronModules[idx1], electronChannels[idx1]};
            const auto &pos1 = dm.sis2pos.at(sis1);
            if (!Mu::EnergyCuts::ApplyCut(ec.positron.at(pos1), cuts, electronEnergies[idx1])) {
                continue;
            }
            hElectronEnergyPPnoVeto->Fill(electronEnergies[idx1], pos2histo.at(pos1));
            hElectronVsEntranceDiffPPnoVeto->Fill(electronTimes[idx1], pos2histo.at(pos1));
            hElectronEnergyVsEntranceDiffPPnoVeto->Fill(electronTimes[idx1], pos2histo.at(pos1),
                                                        electronEnergies[idx1]);
            if (pos1.distance != Mu::FarDetector) {
                continue;
            }
            const auto &posFar = pos1;
            const auto idxFar = idx1;
            for (std::size_t idx2 = 0; idx2 < electronModules.size(); ++idx2) {
                const Mu::SisDetector sis2{electronModules[idx2], electronChannels[idx2]};
                const auto &pos2 = dm.sis2pos.at(sis2);
                if (!Mu::EnergyCuts::ApplyCut(ec.positron.at(pos2), cuts, electronEnergies[idx2])) {
                    continue;
                }
                if ((pos2.distance == Mu::CloseDetector) && (pos2.direction == pos1.direction)) {
                    const auto &posClose = pos2;
                    const auto idxClose = idx2;
                    const float tDiff = electronTimes[idxFar] - electronTimes[idxClose];
                    if ((T_POSITRON_COINCIDENCE_MIN < tDiff) && (tDiff < T_POSITRON_COINCIDENCE_MAX)) {
                        if (posClose.layer == posFar.layer) {
                            hElectronVsEntranceDiff_CoincidencePPnoVeto->Fill(electronTimes[idxFar],
                                                                              pos2histo.at(posFar));
                            hElectronVsEntranceDiff_CoincidencePPnoVeto->Fill(electronTimes[idxClose],
                                                                              pos2histo.at(posClose));
                            hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto->Fill(electronTimes[idxFar],
                                                                                    pos2histo.at(posFar),
                                                                                    electronEnergies[idxFar]);
                            hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto->Fill(electronTimes[idxClose],
                                                                                    pos2histo.at(posClose),
                                                                                    electronEnergies[idxClose]);
                            hElectronVsElectronEnergy_CoincidencePPnoVeto->Fill(electronEnergies[idxFar],
                                                                                pos2histo.at(posFar),
                                                                                electronEnergies[idxClose]);
                            hElectronVsElectronTime_CoincidencePPnoVeto->Fill(tDiff, pos2histo.at(posFar));
                            if (!atomicTimes.empty()) {
                                hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic->Fill(electronTimes[idxFar],
                                                                                        pos2histo.at(posFar));
                            }
                        } else if ((posFar.layer - posClose.layer) == 1) {
                            hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto->Fill(electronTimes[idxFar],
                                                                                             pos2histo.at(posFar));
                        } else if ((posFar.layer - posClose.layer) == 2) {
                            hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto->Fill(electronTimes[idxFar],
                                                                                             pos2histo.at(posFar));
                        } else if ((posFar.layer - posClose.layer) == 3) {
                            hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto->Fill(electronTimes[idxFar],
                                                                                             pos2histo.at(posFar));
                        } else if ((posFar.layer - posClose.layer) == -1) {
                            hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto->Fill(electronTimes[idxFar],
                                                                                               pos2histo.at(posFar));
                        } else if ((posFar.layer - posClose.layer) == -2) {
                            hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto->Fill(electronTimes[idxFar],
                                                                                               pos2histo.at(posFar));
                        } else if ((posFar.layer - posClose.layer) == -3) {
                            hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto->Fill(electronTimes[idxFar],
                                                                                               pos2histo.at(posFar));
                        }
                    }
                }
            }
        }
        for (std::size_t idx = 0; idx < atomicTimes.size(); ++idx) {
            hAtomicElectronVsEntranceDiffPPnoVeto->Fill(atomicTimes[idx], 0);
            hAtomicEnergyPPnoVeto->Fill(atomicEnergies[idx], 0);
        }
        for (const auto &energy: muonEnergies) {
            hMuonEnergyPPnoVeto->Fill(energy, 0);
        }
    };

    ROOT::TThreadedObject<TH2F> hElectronEnergy("hElectronEnergy", "", E_NBINS, E_MIN, E_MAX, nPosDet, posDetBinMin,
                                                posDetBinMax);
    hElectronEnergy->GetXaxis()->SetTitle(energyAxis.c_str());
    labelYaxis(hElectronEnergy, histoPosDetBinLabels);
    ROOT::TThreadedObject<TH2F> hMuonEnergy("hMuonEnergy", "", E_NBINS, E_MIN, E_MAX, 1, -.5, .5);
    hMuonEnergy->GetXaxis()->SetTitle(energyAxis.c_str());
    labelYaxis(hMuonEnergy, histoEntranceLabel);
    auto fillNonPPHistos = [&detectorMaps, &energyCuts, &cuts, &pos2histo, &hElectronEnergy, &hMuonEnergy](
            const int runNumber,
            const std::vector<unsigned short> &electronModules,
            const std::vector<unsigned short> &electronChannels,
            const std::vector<float> &electronEnergies,
            const std::vector<float> &muonTimes,
            const std::vector<float> &muonEnergies) {
        const auto &dm = detectorMaps.GetMap(runNumber);
        const auto &ec = energyCuts.GetCut(runNumber);
        for (std::size_t idx = 0; idx < electronModules.size(); ++idx) {
            const Mu::SisDetector sis{electronModules[idx], electronChannels[idx]};
            const auto &pos = dm.sis2pos.at(sis);
            if (Mu::EnergyCuts::ApplyCut(ec.positron.at(pos), cuts, electronEnergies[idx])) {
                hElectronEnergy->Fill(electronEnergies[idx], pos2histo.at(pos));
            }
        }
        // Only use the seed hit of the event
        for (std::size_t idx = 0; idx < muonTimes.size(); ++idx) {
            if (muonTimes[idx] == 0.) {
                hMuonEnergy->Fill(muonEnergies[idx], 0);
            }
        }
    };


    ROOT::RDataFrame df("EventTree", in_filenames);
    df.Filter("goodEvent").Foreach(fillPPnoVetoHistos,
                                   {"runNumber", "electronModules", "electronChannels", "electronTimes",
                                    "electronEnergies", "atomicTimes", "atomicEnergies", "muonEnergies"});
    df.Foreach(fillNonPPHistos,
               {"runNumber", "electronModules", "electronChannels", "electronEnergies", "muonTimes", "muonEnergies"});
    // The online display expects these histos two be 2D in case of multiple detectors.
    // We have only one, so just hard-code it for the first bin to avoid the need for another detector map.
    auto &&hAtomicEnergy = df.Define("ybin", "0").Histo2D({"hAtomicEnergy", "", E_NBINS, E_MIN, E_MAX, 1, -.5, .5},
                                                          "atomicEnergies", "ybin");
    hAtomicEnergy->GetXaxis()->SetTitle(energyAxis.c_str());
    labelYaxis(hAtomicEnergy, histoAtomicLabel);

    // Merge, save, and delete the threaded histograms
    TFile outfile(out_filename.c_str(), "RECREATE");
    hElectronVsEntranceDiffPPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CoincidencePPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CoincidencePPnoVetoAtomic.Merge()->Write();
    hElectronVsEntranceDiff_CrossCoincidence_upstream1PPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CrossCoincidence_upstream2PPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CrossCoincidence_upstream3PPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CrossCoincidence_downstream1PPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CrossCoincidence_downstream2PPnoVeto.Merge()->Write();
    hElectronVsEntranceDiff_CrossCoincidence_downstream3PPnoVeto.Merge()->Write();
    hElectronEnergy.Merge()->Write();
    hElectronEnergyPPnoVeto.Merge()->Write();
    hElectronEnergyVsEntranceDiffPPnoVeto.Merge()->Write();
    hElectronEnergyVsEntranceDiff_CoincidencePPnoVeto.Merge()->Write();
    hElectronVsElectronEnergy_CoincidencePPnoVeto.Merge()->Write();
    hElectronVsElectronTime_CoincidencePPnoVeto.Merge()->Write();
    hAtomicElectronVsEntranceDiffPPnoVeto.Merge()->Write();
    // Produced via ROOT::RDataFrame::Histo2D, no need to merge
    hAtomicEnergy->Write();
    hAtomicEnergyPPnoVeto.Merge()->Write();
    hMuonEnergy.Merge()->Write();
    hMuonEnergyPPnoVeto.Merge()->Write();
    outfile.Close();
}


void printUsage() {
    std::cerr << "Usage: " << program_invocation_short_name << " NUMTHREADS OUTFILEPREFIX INFILE(S)..." << std::endl;
}


int main(const int argc, const char *argv[]) {
    if (argc < 4) {
        printUsage();
        exit(1);
    }

    const unsigned numThreads = std::stoul(argv[1]);
    std::string out_filename(argv[2]);
    std::vector<std::string> in_filenames;
    for (std::size_t idx = 3; idx < argc; ++idx) {
        in_filenames.emplace_back(argv[idx]);
    }

    for (const auto &cut: cutSequence) {
        std::cout << "Performing " << cut.second << " analysis..." << std::endl;
        muTreeAnal(numThreads, in_filenames, (out_filename + "_" + cut.second + ".root"), cut.first);
    }

    return 0;
}
