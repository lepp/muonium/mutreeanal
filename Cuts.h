//
// Created by damian on 25.09.21.
//

#ifndef MUTREEANAL_CUTS_H
#define MUTREEANAL_CUTS_H


#include <array>
#include <iostream>
#include <limits>
#include <map>
#include <stdexcept>
#include <vector>

#include "Detector.h"


#define E_LOCUT 1
#define E_HICUT 2


namespace Mu {
    typedef std::map<PositronDetector, std::array<float, 2>> PositronEnergyCut;


    typedef struct EnergyCut {
        int runNumberMin = std::numeric_limits<int>::min();
        int runNumberMax = std::numeric_limits<int>::max();
        PositronEnergyCut positron{};
        std::array<float, 2> entrance{0};
        std::array<float, 2> atomic{0};
    } EnergyCut;


    class EnergyCuts {
    public:
        EnergyCuts() {
            // Cuts for beam time 2021 slanted target
            energyCuts.push_back(EnergyCut{
                    .runNumberMin = 2000,
                    .runNumberMax = 2400,
                    .positron = {
                            {
                                    {1, LeftDetector,  CloseDetector},
                                    {153, 541}
                            },
                            {
                                    {1, LeftDetector,  FarDetector},
                                    {152, 540}
                            },
                            {
                                    {2, LeftDetector,  CloseDetector},
                                    {165, 583}
                            },
                            {
                                    {2, LeftDetector,  FarDetector},
                                    {104, 368}
                            },
                            {
                                    {3, LeftDetector,  CloseDetector},
                                    {86,  305}
                            },
                            {
                                    {3, LeftDetector,  FarDetector},
                                    {140, 498}
                            },
                            {
                                    {4, LeftDetector,  CloseDetector},
                                    {143, 508}
                            },
                            {
                                    {4, LeftDetector,  FarDetector},
                                    {925, 3270}
                            },
                            {
                                    {1, RightDetector, CloseDetector},
                                    {115, 408}
                            },
                            {
                                    {1, RightDetector, FarDetector},
                                    {53,  187}
                            },
                            {
                                    {2, RightDetector, CloseDetector},
                                    {106, 375}
                            },
                            {
                                    {2, RightDetector, FarDetector},
                                    {238, 841}
                            },
                            {
                                    {3, RightDetector, CloseDetector},
                                    {147, 522}
                            },
                            {
                                    {3, RightDetector, FarDetector},
                                    {105, 373}
                            },
                            {
                                    {4, RightDetector, CloseDetector},
                                    {34,  121}
                            },
                            {
                                    {4, RightDetector, FarDetector},
                                    {352, 1245}
                            },
                    },
                    .entrance = {
                            std::numeric_limits<float>::min(),
                            std::numeric_limits<float>::max()
                    },
                    .atomic = {
                            std::numeric_limits<float>::min(),
                            std::numeric_limits<float>::max()
                    }
            });

            // Cuts for beam time 2021 vertical target
            energyCuts.push_back(EnergyCut{
                    .runNumberMin = 2401,
                    .runNumberMax = std::numeric_limits<int>::max(),
                    .positron = {
                            {
                                    {1, LeftDetector,  CloseDetector},
                                    {74,  263}
                            },
                            {
                                    {1, LeftDetector,  FarDetector},
                                    {152, 540}
                            },
                            {
                                    {2, LeftDetector,  CloseDetector},
                                    {89,  315}
                            },
                            {
                                    {2, LeftDetector,  FarDetector},
                                    {104, 368}
                            },
                            {
                                    {3, LeftDetector,  CloseDetector},
                                    {59,  210}
                            },
                            {
                                    {3, LeftDetector,  FarDetector},
                                    {112, 398}
                            },
                            {
                                    {4, LeftDetector,  CloseDetector},
                                    {185, 654}
                            },
                            {
                                    {4, LeftDetector,  FarDetector},
                                    {265, 938}
                            },
                            {
                                    {1, RightDetector, CloseDetector},
                                    {100, 620}
                            },
                            {
                                    {1, RightDetector, FarDetector},
                                    {33,  119}
                            },
                            {
                                    {2, RightDetector, CloseDetector},
                                    {110, 520}
                            },
                            {
                                    {2, RightDetector, FarDetector},
                                    {29,  150}
                            },
                            {
                                    {3, RightDetector, CloseDetector},
                                    {9,   340}
                            },
                            {
                                    {3, RightDetector, FarDetector},
                                    {61,  215}
                            },
                            {
                                    {4, RightDetector, CloseDetector},
                                    {24,  87}
                            },
                            {
                                    {4, RightDetector, FarDetector},
                                    {261, 922}
                            },
                    },
                    .entrance = {
                            std::numeric_limits<float>::min(),
                            std::numeric_limits<float>::max()
                    },
                    .atomic = {
                            std::numeric_limits<float>::min(),
                            std::numeric_limits<float>::max()
                    }
            });
        }

        [[nodiscard]] const EnergyCut &GetCut(const int runNumber) const {
            for (const auto &ec: energyCuts) {
                if ((ec.runNumberMin <= runNumber) && (runNumber <= ec.runNumberMax)) {
                    return ec;
                }
            }
            throw std::runtime_error("No energy cuts implemented for run " + std::to_string(runNumber));
        }

        static bool ApplyCut(const std::array<float, 2> &cut, const int cutType, const float energy) {
            bool pass = true;
            if (cutType & E_LOCUT) {
                pass &= (energy > cut[0]);
            }
            if (cutType & E_HICUT) {
                pass &= (energy < cut[1]);
            }
            return pass;
        }

        void Print() const {
            std::cout << "Energy cuts:" << std::endl;
            for (const auto &ec: energyCuts) {
                std::cout << "Run: " << ec.runNumberMin << "," << ec.runNumberMax << std::endl;
                for (const auto &[pos, energy]: ec.positron) {
                    std::cout << DetectorMaps::GetPosName(pos) << ": " << energy[0] << "," << energy[1] << std::endl;
                }
                std::cout << "Entrance: " << ec.entrance[0] << "," << ec.entrance[1] << std::endl;
                std::cout << "Atomic: " << ec.atomic[0] << "," << ec.atomic[1] << std::endl;
            }
        }


    private:
        std::vector<EnergyCut> energyCuts;
    };
}


#endif //MUTREEANAL_CUTS_H
