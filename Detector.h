//
// Created by damian on 23.09.21.
//


#ifndef MUTREEANAL_DETECTOR_H
#define MUTREEANAL_DETECTOR_H


#include <cstdlib>
#include <iostream>
#include <limits>
#include <map>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>


#define N_POSITRON_LAYERS 4


namespace Mu {
    enum DetectorDirection {
        LeftDetector,
        RightDetector
    };


    enum DetectorDistance {
        CloseDetector,
        FarDetector
    };


    typedef struct SisDetector {
        std::size_t module = 0;
        std::size_t channel = 0;
    } SisDetector;

    bool operator<(const SisDetector &lhs, const SisDetector &rhs) {
        return std::tie(lhs.module, lhs.channel) < std::tie(rhs.module, rhs.channel);
    }

    bool operator==(const SisDetector &lhs, const SisDetector &rhs) {
        return std::tie(lhs.module, lhs.channel) == std::tie(rhs.module, rhs.channel);
    }


    typedef struct PositronDetector {
        std::size_t layer = 0;
        DetectorDirection direction = LeftDetector;
        DetectorDistance distance = CloseDetector;
    } PositronDetector;

    // Order first by direction, then layer, then distance
    bool operator<(const PositronDetector &lhs, const PositronDetector &rhs) {
        return std::tie(lhs.direction, lhs.layer, lhs.distance) < std::tie(rhs.direction, rhs.layer, rhs.distance);
    }

    bool operator==(const PositronDetector &lhs, const PositronDetector &rhs) {
        return std::tie(lhs.direction, lhs.layer, lhs.distance) == std::tie(rhs.direction, rhs.layer, rhs.distance);
    }


    typedef std::map<SisDetector, PositronDetector> Sis2Pos;
    typedef std::map<PositronDetector, SisDetector> Pos2Sis;


    typedef struct DetectorMap {
        int runNumberMin = std::numeric_limits<int>::min();
        int runNumberMax = std::numeric_limits<int>::max();
        Pos2Sis pos2sis;
        Sis2Pos sis2pos;
        SisDetector entrance{};
        SisDetector atomic{};
    } DetectorMap;


    class DetectorMaps {
    public:
        DetectorMaps() {
            DetectorMap dm;
            // Map for beam time 2021
            dm.runNumberMin = 2000;
            for (std::size_t l = 1; l <= N_POSITRON_LAYERS; ++l) {
                dm.pos2sis[{l, LeftDetector, CloseDetector}] = {0, (l * 2 - 1)};
                dm.pos2sis[{l, LeftDetector, FarDetector}] = {0, (l * 2)};
                dm.pos2sis[{l, RightDetector, CloseDetector}] = {1, (l * 2 - 1)};
                dm.pos2sis[{l, RightDetector, FarDetector}] = {1, (l * 2)};
            }
            dm.sis2pos = InvertMap(dm.pos2sis);
            dm.entrance = {0, 9};
            dm.atomic = {1, 9};
            detectorMaps.push_back(dm);
        }

        [[nodiscard]] const DetectorMap &GetMap(const int runNumber) const {
            for (const auto &dm: detectorMaps) {
                if ((dm.runNumberMin <= runNumber) && (runNumber <= dm.runNumberMax)) {
                    return dm;
                }
            }
            throw std::runtime_error("No detector map implemented for run " + std::to_string(runNumber));
        }

        void Print() const {
            std::cout << "Detector maps:" << std::endl;
            for (const auto &dm: detectorMaps) {
                std::cout << "Run: " << dm.runNumberMin << "," << dm.runNumberMax << std::endl;
                for (const auto &[pos, sis]: dm.pos2sis) {
                    std::cout << GetPosName(pos) << ": " << sis.module << "," << sis.channel << std::endl;
                }
                for (const auto &[sis, pos]: dm.sis2pos) {
                    std::cout << sis.module << "," << sis.channel << ": " << GetPosName(pos) << std::endl;
                }
                std::cout << "Entrance: " << dm.entrance.module << "," << dm.entrance.channel << std::endl;
                std::cout << "Atomic: " << dm.atomic.module << "," << dm.atomic.channel << std::endl;
            }
        }

        static Sis2Pos InvertMap(const Pos2Sis &pos2sis) {
            Sis2Pos sis2pos;
            for (const auto &[pos, sis]: pos2sis) {
                sis2pos[sis] = pos;
            }

            return sis2pos;
        }

        static std::string GetPosName(const PositronDetector &pos) {
            std::string name = std::to_string(pos.layer);
            name += (pos.direction == LeftDetector) ? "L" : "R";
            name += (pos.distance == CloseDetector) ? "C" : "F";

            return name;
        }


    private:
        std::vector<DetectorMap> detectorMaps;
    };
}


#endif //MUTREEANAL_DETECTOR_H
